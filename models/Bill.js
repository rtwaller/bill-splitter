module.exports = (sequelize, DataTypes) =>
  sequelize.define("Bill", {
    friends: [],
    total: {
      type: DataTypes.INTEGER,
      unique: true
    }
  });
